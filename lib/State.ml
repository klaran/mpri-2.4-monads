(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)

module Make (S: sig
                 type t
                 val init : t
               end) = struct

  module State = struct
    type 'a t = S.t -> 'a * S.t

    let return a = fun state -> (a, state)

    let bind m f = fun state ->
      let (a, s) = m state in f a s

  end

  module M = Monad.Expand (State)
  include M

  let get () s = (s,s)

  let set x _ = ((), x)

  let run m = fst @@ m S.init

end
